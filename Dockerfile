# root image
FROM node

# root folder
WORKDIR /apitechu

# copy project files
ADD . /apitechu

# install needed packages
RUN npm install --only-prod

# open port
EXPOSE 3000

# init command
CMD ["node", "server.js"]
