const io = require('../io');
const requestJson = require('request-json');
const baseMLabURL =
  'https://api.mlab.com/api/1/databases/apitechucsa13ed/collections/';
const mLabAPIKey = 'apiKey=' + process.env.MLAB_API_KEY;

const crypt = require('../crypt');

function getAccountsByIdV2(req, res) {
  console.log('GET /apitechu/v2/accounts/:id');

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log('Client created');

  // prevent sql injection
  var id = Number.parseInt(req.params.id);
  console.log('id: ' + id);
  var query = 'q=' + JSON.stringify({id: id});

  httpClient.get('accounts?' + mLabAPIKey + '&' + query, function(err, resMlab, body) {
    if (err) {
      var response = {msg: 'Error al recuperar las cuentas'};
      res.status(500);
    } else {
      if (body.length > 0) {
        var response = body;
      } else {
        var response = {msg: 'Usuario sin cuentas'};
        res.status(404);
      }
    }

    res.send(response);
  });
}
module.exports.getAccountsByIdV2 = getAccountsByIdV2;
