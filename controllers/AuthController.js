const io = require('../io');
const requestJson = require('request-json');
const baseMLabURL =
  'https://api.mlab.com/api/1/databases/apitechucsa13ed/collections/';
const mLabAPIKey = 'apiKey=' + process.env.MLAB_API_KEY;

const crypt = require('../crypt');

function loginUserV1(req, res) {
  console.log('POST /apitechu/v1/login');

  var users = require('../usuarios.json');

  for (i = 0; i < users.length; i++) {
    if (
      users[i].email == req.body.email &&
      users[i].password == req.body.password
    ) {
      // login ok
      users[i].logged = true;
      console.log('logged user: ' + users[i].id + ' -> ' + users[i].logged);
      io.writeUserDataToFile(users);
      res.send({"msg": 'login correcto', idUsuario: users[i].id});
    }
  }

  // user/password not found
  console.log(
    'logged user/password not found -> ' +
      req.body.email +
      ' / ' +
      req.body.password
  );
  res.send({"msg": 'login incorrecto'});
}

function logoutUserV1(req, res) {
  console.log('POST /apitechu/v1/logout/:id');

  var users = require('../usuarios.json');

  for (i = 0; i < users.length; i++) {
    if (users[i].id == req.params.id && users[i].logged) {
      // logout ok
      delete users[i].logged;
      console.log('unlogged user: ' + users[i].id);
      io.writeUserDataToFile(users);
      res.send({"msg": 'logout correcto', idUsuario: users[i].id});
    }
  }

  // user not logged in
  console.log('user not logged in -> ' + req.params.id);
  res.send({"msg": 'logout incorrecto'});
}

function loginUserV2(req, res) {
  console.log('POST /apitechu/v2/login');

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log('Client created');

  var email = req.body.email;
  var password = req.body.password;

  var query = 'q=' + JSON.stringify({"email": email});
  httpClient.get('user?' + mLabAPIKey + '&' + query, function(err, resMlab, body) {
    if (err) {
      var response = {"msg": 'Error al recuperar el usuario'};
      res.status(500);
    } else {

      if (body.length > 0) {
        // email found
        console.log('email found: ' + email);
        if (crypt.checkPassword(password, body[0].password)) {
          // valid password
          console.log('valid password: ' + password + '/' + body[0].password);
          console.log('_id: ' + body[0]._id.$oid);
          httpClient.put('user/' + body[0]._id.$oid + '?' + mLabAPIKey, {"$set":{"logged":true}}, function(errPut, resMlabPut, bodyPut) {
            console.log(bodyPut);
            if (errPut) {
              var response = {"msg": 'Error al recuperar el usuario'};
              res.status(500);
            } else {
              var response = {"msg": 'login correcto', "idUsuario": bodyPut.id};
            }
            res.send(response);
          });

        } else {
          var response = {"msg": 'login incorrecto'};
          res.send(response);
        }
      } else {
        // email not found
        var response = {"msg": 'Usuario no encontrado'};
        res.status(404);
        res.send(response);
      }
    }
  });
}

function logoutUserV2(req, res) {
  console.log('POST /apitechu/v2/logout/:id');

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log('Client created');

  // prevent sql injection
  var id = Number.parseInt(req.params.id);
  console.log('id: ' + id);
  var query = 'q=' + JSON.stringify({id: id});

  httpClient.get('user?' + mLabAPIKey + '&' + query, function(err, resMlab, body) {
    if (err) {
      var response = {"msg": 'Error al recuperar el usuario'};
      res.status(500);
    } else {

      if (body.length > 0) {
        // id found
        console.log('id found: ' + id);
        console.log('_id: ' + body[0]._id.$oid);
        httpClient.put('user/' + body[0]._id.$oid + '?' + mLabAPIKey, {"$unset":{"logged":""}}, function(errPut, resMlabPut, bodyPut) {
          console.log(bodyPut);
          if (errPut) {
            var response = {"msg": 'Error al recuperar el usuario'};
            res.status(500);
          } else {
            var response = {"msg": 'logout correcto', "idUsuario": bodyPut.id};
          }
          res.send(response);
        });

      } else {
        var response = {"msg": 'logout incorrecto'};
        res.send(response);
      }
    }
  });
}

module.exports.loginUserV1 = loginUserV1;
module.exports.loginUserV2 = loginUserV2;
module.exports.logoutUserV2 = logoutUserV2;
module.exports.logoutUserV1 = logoutUserV1;
