const io = require('../io');
const requestJson = require('request-json');
const baseMLabURL =
  'https://api.mlab.com/api/1/databases/apitechucsa13ed/collections/';
const mLabAPIKey = 'apiKey=' + process.env.MLAB_API_KEY;

const crypt = require('../crypt');

function getUsersV1(req, res) {
  console.log('GET /apitechu/v1/users');

  var result = {};
  var users = require('../usuarios.json');

  if (req.query.$count == 'true') {
    console.log('Count needed');

    result.count = users.length;
  }

  result.users = req.query.$top ? users.slice(0, req.query.$top) : users;
  res.send(result);
}

function getUsersV2(req, res) {
  console.log('GET /apitechu/v2/users');

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log('Client created');

  httpClient.get('user?' + mLabAPIKey, function(err, resMlab, body) {
    var response = !err
      ? body
      : {
          msg: 'Error obteniendo usuarios.',
        };
    res.send(response);
  });
}

function getUserByIdV2(req, res) {
  console.log('GET /apitechu/v2/users/:id');

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log('Client created');

  // prevent sql injection
  var id = Number.parseInt(req.params.id);
  console.log('id: ' + id);
  var query = 'q=' + JSON.stringify({id: id});

  httpClient.get('user?' + mLabAPIKey + '&' + query, function(err, resMlab, body) {
    if (err) {
      var response = {msg: 'Error al recuperar el usuario'};
      res.status(500);
    } else {
      if (body.length > 0) {
        var response = body[0];
      } else {
        var response = {msg: 'Usuario no encontrado'};
        res.status(404);
      }
    }

    res.send(response);
  });
}

function createUserV1(req, res) {
  console.log('POST /apitechu/v1/users');
  console.log(req.headers);
  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);

  var newUser = {
    first_name: req.body.first_name,
    last_name: req.body.last_name,
    email: req.body.email,
  };

  console.log(newUser);

  var users = require('../usuarios.json');
  users.push(newUser);
  console.log('Usuario añadido al array');

  io.writeUserDataToFile(users);
  console.log('Proceso de creación de usuario finalizado');
}

function createUserV2(req, res) {
  console.log('POST /apitechu/v2/users');

  console.log(req.body.id);
  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);
  //OJO quitar
  console.log(req.body.password);

  var newUser = {
    id: req.body.id,
    first_name: req.body.first_name,
    last_name: req.body.last_name,
    email: req.body.email,
    password: crypt.hash(req.body.password),
  };

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log('Client created');

  httpClient.post('user?' + mLabAPIKey, newUser, function(err, resMLab, body) {
    console.log('Usuario creado en Mlab');
    res.status(201).send({msg: 'Usuario creado'});
  });
}

function deleteUserV1(req, res) {
  console.log('DELETE /apitechu/v1/users/:id');
  console.log('id del usuario a borrar es ' + req.params.id);

  var users = require('../usuarios.json');
  var i = 0;

  // for(i = 0; i < users.length; i++) {
  //   if (users[i].id == req.params.id){
  //     users.splice(i,1);
  //   }
  // }

  // for (user in users){
  //   if (users[user].id == req.params.id){
  //       users.splice(user,1);
  //     }
  // }

  // users.forEach(function(element) {
  //     if (element.id == req.params.id){
  //         users.splice(users.indexOf(element),1);
  //       }
  //   }
  //);

  // users.splice(users.findIndex(function(element){
  //     return element.id == req.params.id
  //   }),1);

  // for (user of users){
  //   if (user.id == req.params.id){
  //       users.splice(users.indexOf(user),1);
  //     }
  // }

  for (user of users.entries()) {
    if (user[1].id == req.params.id) {
      users.splice(user[0], 1);
    }
  }

  console.log('Usuario quitado del array');

  io.writeUserDataToFile(users);
}

function deleteUserV2(req, res) {
  console.log('PUT /apitechu/v2/users/:id');

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log('Client created');

  // prevent sql injection
  var id = Number.parseInt(req.params.id);
  console.log('id: ' + id);
  var query = 'q=' + JSON.stringify({id: id});

  httpClient.put('user?' + mLabAPIKey + '&' + query, [{}], function(err, resMlab, body) {
    if (err) {
      var response = {msg: 'Error al recuperar el usuario'};
      res.status(500);
    } else {
      var response = {
        msg: 'Usuario eliminado',
        id: id,
      };
    }
    res.send(response);
  });
}

module.exports.getUsersV1 = getUsersV1;
module.exports.getUsersV2 = getUsersV2;
module.exports.getUserByIdV2 = getUserByIdV2;
module.exports.createUserV1 = createUserV1;
module.exports.createUserV2 = createUserV2;
module.exports.deleteUserV1 = deleteUserV1;
module.exports.deleteUserV2 = deleteUserV2;
