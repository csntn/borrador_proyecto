require('dotenv').config();
const express = require('express');
const app = express();

var enableCORS = function(req, res, next) {

    res.set("Access-Control-Allow-Origin", "*");
    res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
    res.set("Access-Control-Allow-Headers", "Content-Type");
    next();

};

const io = require('./io');
const userController = require('./controllers/UserController');
const authController = require('./controllers/AuthController');
const accountController = require('./controllers/AccountController');

app.use(express.json());
app.use(enableCORS);

const port = process.env.PORT || 3000;
app.listen(port);
console.log('API escuchando en el puerto ' + port);

app.get('/apitechu/v1/hello', function(req, res) {
    console.log('GET /apitechu/v1/hello');

    res.send({msg: 'Hola desde API TechU'});
});

app.get('/apitechu/v1/users', userController.getUsersV1);
app.get('/apitechu/v2/users', userController.getUsersV2);
app.get('/apitechu/v2/users/:id', userController.getUserByIdV2);
app.post('/apitechu/v1/users', userController.createUserV1);
app.post('/apitechu/v2/users', userController.createUserV2);
app.delete('/apitechu/v1/users/:id', userController.deleteUserV1);
//OJO en mlab borrar es con put o con delete por _id
app.put('/apitechu/v2/users/:id', userController.deleteUserV2);
app.post('/apitechu/v1/login', authController.loginUserV1);
app.post('/apitechu/v1/logout/:id', authController.logoutUserV1);
app.post('/apitechu/v2/login', authController.loginUserV2);
app.post('/apitechu/v2/logout/:id', authController.logoutUserV2);
app.get('/apitechu/v2/accounts/:id', accountController.getAccountsByIdV2);
